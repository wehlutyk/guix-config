(use-modules
 (gnu packages))

(specifications->manifest
 ;; emacs & mail
 '("emacs" "emacs-guix"
   ;"emacs-pdf-tools"
   "isync" "mu" "altermime" "graphviz"
   ;; sway desktop environment
   "waybar" "swaylock" "swaybg" "swayidle" "wl-clipboard" "rofi" "rofi-pass" "xset" "slurp" "grim" "redshift-wayland" "pinentry" "pinentry-gnome3" "udiskie" "pavucontrol" "mako" "libnotify" "xrdb" "xdg-utils" "xrandr" "arandr" "xev" "wev"
   ;; Gnome desktop environment
   "gnome-tweaks"
   "gnome-shell-extension-paperwm"
   ;; input
   ;"ibus" "ibus-anthy"
   ;; printing
   ;"cups"
   ;; fonts
   ;"font-awesome" "font-hack" "font-adobe-source-han-sans" "font-adobe-source-sans-pro" "font-adobe-source-code-pro" "font-wqy-zenhei" "font-mplus-testflight" "font-google-roboto" "font-google-noto" "font-liberation" "font-dejavu" "font-ipa-mj-mincho" "font-mathjax" "font-linuxlibertine" "font-jetbrains-mono" "font-tex-gyre"
   ;; web
   ;"icecat"
   "browserpass-native"
   "firefox-wayland"  ; nonguix
   ;; codecs & video
   "ffmpeg"
   "gstreamer" "gst-plugins-base" "gst-plugins-good" "gst-plugins-bad" "gst-plugins-ugly" "gst-libav"
   ;; FIXME "opencv"
   ;; compiling
   "make" "automake" "autoconf" "gcc-toolchain" "pkg-config" "openssl"
   ;; office
   "libreoffice"
   "simple-scan"
   "hunspell" "hunspell-dict-en-us" "hunspell-dict-en-gb" "hunspell-dict-fr-toutesvariantes"
   "aspell" "aspell-dict-fr" "aspell-dict-en" "aspell-dict-es"
   "homebank"
   ;"texlive" "biber"
   "pandoc"
   ;; sync/backup/files
   "syncthing" "borg"
   "baobab"
   "meld" "diffoscope"
   ;; terminal
   "kakoune" "fish" "tmux"
   ;"screen"
   "git" "git:gui" "git:send-email" "pass-git-helper"
   "tree" "ripgrep" "fasd" "go-github-com-junegunn-fzf" "trash-cli"
   ;"pwgen"
   "password-store" "gnupg"
   "jq" "wget" "httpie" "curlpp"
   "bc"
   "file" "unzip"
   ;; admin
   "openssh" "nmap" "rsync" "cryptsetup" "iftop" "htop" "btrfs-progs" "dosfstools" "virt-manager"
   "flatpak"
   ;"xdg-desktop-portal" "xdg-desktop-portal-gtk" "parted" "iputils" "seahorse" "tcpdump" "lshw" "bind:utils" "docker-cli" "strace"
   ;; bluetooth
   ;"blueman"
   ;; vpn
   "openconnect" "ocproxy" "network-manager-openconnect"
   ;; hardware hacking
   ;"openocd"
   ;; android
   ;"adb" "fastboot"
   ;; 3D modelling
   ;"blender"
   ;; power-management
   "powertop" "tlp" "smartmontools"
   ;; media
   "yt-dlp" "vlc" "stapler" "gimp" "imagemagick" "inkscape" "audacity" "shotwell"
   ;; programming
   "python" "poetry" "python-jupyterlab" "python-black"
   ;"rust" "rust:cargo" "rust-clippy"
   ;"racket"
   "guix-jupyter"
   "julia"
   ;; webdev
   ;"postgresql"
   "haunt"
   ;"elm-compiler"
   ;; blogging
   ;"pelican" "python-ghp-import"
   ;; unused apps
   ;; "anki" "kicad" "liferea"
   ;; apps
   "transmission:gui"
   ;"kubo"
   ;"okular" "calibre"
   "clementine"
   ;"rhythmbox"
   ))
