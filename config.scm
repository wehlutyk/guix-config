(use-modules
 (gnu)
 (gnu home)
 (gnu home services)
 (gnu home services desktop)
 (gnu home services gnupg)
 (gnu home services shells)
 (gnu home services sound)
 (gnu home services syncthing)
 (gnu services guix)
 (guix channels)
 (guix records)
 (nongnu packages linux)
 (nongnu system linux-initrd)
 )
(use-package-modules
 android
 certs
 cups
 freedesktop
 gnupg
 hardware
 linux
 package-management
 shells
 )
(use-service-modules
 ;; TODO: for future fingerprint once it works everywhere
 ;; https://knowledgebase.frame.work/fingerprint-enrollment-rkG6YP7xF
 ;; https://community.frame.work/t/guide-lvfs-fingerprint-reader-firmware-for-framework-laptop-13-13th-gen-intel-core-amd-7040-series-fingerprint-reader-on-fedora-39-and-ubuntu-22-04/41898/2
 ;;authentication
 cups
 dbus
 desktop
 docker
 linux
 shepherd
 ssh
 virtualization
 xorg
 )

;; TODO
;; - home-dotfiles-service-type

(define sl-home-environment
  (home-environment
   ;; Below is the list of packages that will show up in your
   ;; Home profile, under ~/.guix-home/profile.
   (packages (specifications->packages
              (list
               ;; Gnome additions
               "gnome-shell-extension-paperwm"
               "gnome-shell-extension-clipboard-indicator"
               "gnome-shell-extension-appindicator"
               "gnome-shell-extensions"
               "gnome-tweaks"
               ;"gnome-boxes"
               ;; XXX Provides a temporary fix for https://issues.guix.gnu.org/72448
               ;; XXX "glib:bin" gives "gio" which works in the shell,
               ;; but for unknown reason not from emacs (whereas is reaches gnome-vfs)
                                        ;"glib:bin"
               "gnome-vfs"

               ;; Emacs
               "emacs"
               "fd"
               "emacs-guix"
               "perl"  ; Useful for  grep/sed-like in Emacs
                                        ;"emacs-pdf-tools"
               "isync"
               "mu"
               "bogofilter"
               "altermime"
               "graphviz"
               "gnutls"  ; For secure IRC connections
               ;; Java runtime at least for
               ;; - org-analyzer
               ;; - ltex-ls, itself for lsp-ltex, needing java 11+
               "openjdk@11"
               ;; Node provides at least
               ;; - elm-format for emacs; with dotfiles/.npmrc and "npm install -g elm-format"
               "node"
               ;; Asked by Doom at
               ;; [[orgit-rev:~/dotfiles/::56fd27c6d0a67537e0edd1a37c562f05c0ca3a2b][~/dotfiles/ (magit-rev 56fd27c)]]
               "tidy-html" "python-jsbeautifier" "shfmt" "shellcheck" "rust-cargo" "rust" "racket" "python" "python-black" "python-pyflakes" "python-isort" "python-nose" "python-pytest" "clang" "glslang"

               ;; Browser
               "browserpass-native"
               "firefox"

               ;; For Web apps such as Microsoft Teams:
               ;; - set up chromium https://divansantana.com/teams-on-linux/
               ;; - allow cookies https://spgeeks.devoworx.com/teams-to-open-the-web-app-change-your-browser-settings/
               "ungoogled-chromium-wayland"

               ;; PDF & Co reading, libraries and tooling
               "poppler-qt5"
               "calibre"

               ;; PDF Audio reading

               ;; Text-to-speech if not an audiobook
               ;; TODO: add coqui-AI https://github.com/coqui-ai/TTS/discussions/2583 with https://discourse.mozilla.org/t/multi-platform-multi-language-docker-images/72080 to first run
               ;; TODO: explore https://github.com/aedocw/epub2tts
               ;; TODO: add MaryTTS? https://marytts.github.io/
               ;; HOLD(old): add mimic3 https://github.com/MycroftAI/mimic3
               ;; TODO: add festvox-us-slt-hts to festival
               ;;   https://askubuntu.com/a/908889
               ;;   http://www.festvox.org/cmu_arctic/dbs_slt.html
               ;;"espeak-ng"
               ;; TODO-NEXT Explore launching "speech-dispatcher -t 0 -s", as that works for Foliate
               ;;"speech-dispatcher"

               ;; Reading-hearing
               ;; FIXME https://lists.gnu.org/archive/html/guix-devel/2024-03/msg00217.html
               "okular"
               ;;"qtspeech" "qtmultimedia"
               ;; Read EPUB3+audio (computer or human)
               ;; TODO: https://thorium.edrlab.org/

               ;; Syncing audio and books ento EPUB3
               ;; TODO: https://smoores.gitlab.io/storyteller/docs/syncing-books/

               ;; Wayland-X11 copy-paste
               "wl-clipboard" "xset" "xclip"

               ;; Printing
               "cups"

               ;; Camera
               "gphotofs"
               "gphoto2"

               ;; Shared files (could use cryfs)
               "onedrive"

               ;; Japanese and Asia fonts
               "font-adobe-source-han-sans"
               "font-adobe-source-sans-pro"
               "font-adobe-source-code-pro"
               "font-wqy-zenhei"
               "font-ipa-mj-mincho"

               ;; More fonts
               ;;"font-mplus-testflight"
               ;;"font-google-roboto"
               ;;"font-google-noto"
               ;;"font-liberation"
               ;;"font-linuxlibertine"
               ;;"font-jetbrains-mono"
               ;;"font-tex-gyre"

               ;; Standard fonts
               ;;"font-dejavu"
               ;;"font-mathjax"

               ;; Coding fonts
               ;;"font-fira-sans" "font-fira-mono" "font-fira-code"
               ;;"font-awesome"
               ;;"font-hack"

               ;; Media
               "ffmpeg"
               "gstreamer"
               "gst-plugins-base"
               "gst-plugins-good"
               "gst-plugins-bad"
               "gst-plugins-ugly"
               "gst-libav"
               "openshot"

               ;; Compiling
               "make"
               "automake"
               "autoconf"
               "gcc-toolchain"
               "pkg-config"

               ;; Working bluetooth
               "bluez"

               ;; Networking VPN
               "openconnect"
               "ocproxy"
               "network-manager-openconnect"

               ;; QR codes
               "qrencode"
               "zbar"

               ;; Spell checking
               ;; TODO: set up OCR, get hunspell-dict-es?, add JP for all
               "hunspell"
               "hunspell-dict-en-us"
               "hunspell-dict-en-gb"
               "hunspell-dict-fr-toutesvariantes"
               "aspell"
               "aspell-dict-fr"
               "aspell-dict-en"
               "aspell-dict-es"

               ;; Shell
               ;;"diffoscope"  ; Build failing
               "tilix"
               "fish"
               "fasd"
               "kakoune"
               "tmux"
               "screen"
               "openssl"
               "git"
               "git:gui"
               "git:send-email"
               "pass-git-helper"
               "tree"
               "ripgrep"
               "go-github-com-junegunn-fzf"
               ;"trash-cli"
               "inotify-tools"
               "pinentry"
               "password-store"
               "gnupg"
               "jq"
               "wget"
               "httpie"
               "curlpp"
               "bc"
               "file"
               "unzip"
               "openssh"
               "nmap"
               "rsync"
               "htop"
               "virt-manager"
               ;;"cryptsetup"
               ;;"iftop"
               ;;"btrfs-progs"
               ;;"dosfstools"
               ;;"parted"
               ;;"iputils"
               ;;"tcpdump"
               ;;"lshw"
               ;;"bind:utils"
               "docker-cli"
               "docker-compose"
               ;;"strace"
               ;;"blueman"

               ;; Code
               ;;"python"
               ;;"poetry"
               ;;"python-black"
               ;;"guix-jupyter"
               ;;"julia"
               ;;"haunt"
               ;;"racket"

               ;; Tooling
               "syncthing"
               "yt-dlp"
               "stapler"
               "imagemagick"
               "pandoc"
               "zip"  ; needed by pandoc for odt
               "borg"
               "texlive" "texlive-latexmk" "texlive-tex-gyre" "texlive-tex-gyre-math"
               ;; Low light screen
               ;; https://community.frame.work/t/resolved-even-lower-screen-brightness/25711
               "brillo"
               ;;"smartmontools"
               "avahi"

               ;; Diagnosis tooling
               "xev"
               "wev"
               "xdg-utils"
               "powertop"
               "lsof"

               ;; Tooling apps
               "meld"
               "seahorse"
               "baobab"
               "homebank"

               ;; Media apps
               "vlc"
               "gimp"
               "audacity"
               "shotwell"
               "inkscape"
               "transmission:gui"
               ;; "calibre"
               "clementine"
               "zoom"
               "libreoffice"
               "simple-scan"

               ;; Health apps
               ;; MRI reading
               ;;"dcmtk"  ; looking for https://dcmtk.org/en/dicomscope/
               ;;"insight-toolkit"
               ;;"itk-snap"
               ;;"gdcm"

               ;; Games
               ;;"hexahop"
               "steam"
               ;; All the Wine-related packages may not be needed
               ;"wine" "wine64" "winetricks" "dxvk"

               ;; Flatpak
               "flatpak"
               "flatpak-xdg-utils"
               "xdg-desktop-portal"
               ;;"xdg-desktop-portal-gtk"

               ;; Screen sharing (maybe just a subpart of these are needed)
               ;; Can be tested using https://mozilla.github.io/webrtc-landing/gum_test.html
               "xdg-desktop-portal-gnome"
               "pipewire"
               "wireplumber"
               )))

   ;; Their own git repo
   ;; - password-store

   ;; dotfiles(git) services
   ;; - .config/emacs
   ;; - .config/doom
   ;; - .config/fish/fish_plugins
   ;; - .config/fish/functions/fisher.fish
   ;; - .config/pass-git-helper
   ;; - .config/onedrive/config
   ;; - .mbsynrc
   ;; - .ssh
   ;; - .gitconfig
   ;; - .mozilla/firefox/8upludgc.default-release/chrome
   ;; - .mozilla/native-messaging-hosts/com.github.browserpass.native.json
   ;; - .local/share/applications/firefox.desktop
   ;; - .local/bin/{orgroaming,pass1line,teams,docker-credential-pass}
   ;; - .docker/config.json
   ;; - .gnupg/gpg-agent.conf
   ;; - .gnupg/dirmngr.conf
   ;; - .npmrc

   ;; TODO
   ;; - Rofi/Wofi/Bemenu
   ;; - `pass -c <name>` with "wl-clipboard" "xset" "xclip"
   ;;   - can be pasted in the terminal with Ctrl-V directly, not Ctrl-Shift-V
   ;;   - can't be pasted outside of the terminal (not in emacs, gnome-text-editor)
   ;;   - whereas Ctrl-Shift-C in terminal, Ctrl-V outside of terminal works
   ;;   - maybe pass-clip?

   ;; TODO
   ;; - fingerprint https://community.frame.work/t/tracking-gnu-guix-system-on-the-framework/22459/6
   ;; - imapnotify (conf in dotfiles, binary packaged in guix) called every few minutes for email updates
   ;; - more configs in dotfiles/.local/share/applications/
   ;; - multi-key shorcuts and more: https://espanso.org/docs/get-started/

   ;; Not solvable
   ;; - gnome shell passwordstore (no better than https://extensions.gnome.org/extension/1093/passwordstore-manager/)

   ;; Below is the list of Home services.  To search for available
   ;; services, run 'guix home search KEYWORD' in a terminal.
   (services
    (list (service home-bash-service-type
                   (home-bash-configuration
                    (aliases '(("grep" . "grep --color=auto")
                               ("ip" . "ip -color=auto")
                               ("ll" . "ls -l")
                               ("ls" . "ls -p --color=auto")))
                    (bashrc (list (local-file
                                   "/home/sl/Code/System/guix-config/.bashrc"
                                   "bashrc")))
                    (bash-profile (list (local-file
                                         "/home/sl/Code/System/guix-config/.bash_profile"
                                         "bash_profile")))))
          (service home-fish-service-type
                   (home-fish-configuration
                    (config
                     ;; TODO: have packaging take care of this, like done for bash and zsh
                     `(,(mixed-text-file
                         "vendor-flatpak.fish"
                         #~(string-append "\
if status --is-login

  source " #$flatpak "/share/fish/vendor_conf.d/flatpak.fish
  source " #$flatpak "/share/fish/vendor_completions.d/flatpak.fish

end\n\n"))
                       ;; TODO: either integrade fisher and its plugins into the guix-config, or implement them directly
                       ,(mixed-text-file
                         "fisher-fasd.fish"
                         "fasd_setup")
                       ))
                    (environment-variables
                     ;; May be useful
                     ;; # Enable local extensions for pass
                     ;; set -gx PASSWORD_STORE_ENABLE_EXTENSIONS true
                     ;; # Make sure gpg-agent knows our tty https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG_002dAGENT.html
                     ;; set -gx GPG_TTY (tty)

                     ;; TODO: wrap environment-variables in a "status --is-login" so they're not doubled
                     '(("PATH" . "$HOME/.local/bin $PATH")
                       ("PATH" . "$HOME/.npm-packages/bin $PATH")
                       ("EDITOR" . "kak")
                       ("fish_greeting" . "")))
                    (aliases
                     '(("epass" . "PASSWORD_STORE_DIR=~/.password-store-eauchat PASSWORD_STORE_GIT=~/.password-store-eauchat pass")
                       ("ecsupass" . "PASSWORD_STORE_DIR=~/.password-store-ecsu PASSWORD_STORE_GIT=~/.password-store-ecsu pass")))
                    (abbreviations
                     ;; TODO: get "beoga" as a variable
                     '(("backup-media" . "borg create -s --progress --compression auto,lzma sl@eauchat.org:/srv/sl/backups::\\{utcnow\\}-randolph-media Media")
                       ("backup-documents" . "borg create -s --progress --compression auto,lzma sl@eauchat.org:/srv/sl/backups::\\{utcnow\\}-beoga-documents ~/Documents")
                       ("backup" . (string-append "borg create -s --progress --compression auto,lzma"
                                                  " sl@eauchat.org:/srv/sl/backups::\\{utcnow\\}-beoga /home/sl/"
                                                  " --exclude-caches"
                                                  " --exclude ~/Videos/Movies"
                                                  " --exclude ~/Code/reMarkable/qemu-arm"
                                                  " --exclude ~/Code/Research/pce-analysis/data-original/ecsu-pce"
                                                  " --exclude ~/Code/Research/pce-data"
                                                  " --exclude ~/Downloads"
                                                  " --exclude ~/.cache"
                                                  " --exclude ~/.config/emacs/.local"
                                                  " --exclude ~/.local/share/gnome-boxes"
                                                  " --exclude ~/.local/share/Trash"
                                                  " --exclude ~/.local/share/flatpak"))
                       ("ga" . "git add")
                       ("gb" . "git branch")
                       ("gba" . "git branch -a")
                       ("gc" . "git commit -v")
                       ("gc!" . "git commit --amend -v")
                       ("gca" . "git commit -a -v")
                       ("gca!" . "git commit -v -a --amend")
                       ("gce" . "git commit --allow-empty -v")
                       ("gce!" . "git commit --amend --allow-empty -v")
                       ("gcmr" . "git checkout master")
                       ("gcmn" . "git checkout main")
                       ("gco" . "git checkout")
                       ("gd" . "git diff")
                       ("gdc" . "git diff --cached")
                       ("gdcw" . "git diff --cached --word-diff")
                       ("gdw" . "git diff --word-diff")
                       ("gk" . "gitk --all --branches")
                       ("gl" . "git pull")
                       ("glg" . "git log --stat")
                       ("gp" . "git push")
                       ("gpoat" . "\"git push origin --all; and git push origin --tags\"")
                       ("gss" . "git status -s")
                       ("gst" . "git status")
                       ("up" . "\"curl -Ffile=@- http://0x0.st\"")
                       ("guix-upgrade" . "\"sudo guix system reconfigure ~/Code/System/guix-config/config.scm; and echo; and date; and echo; and guix home reconfigure ~/Code/System/guix-config/home-configuration.scm; and echo; and date\"")
                       ("orgp" . "\"cd ~/org; git add .; git commit -m 'update'; git push\"")
                       ("pgp" . "pass git push")))))
          (service home-syncthing-service-type
                   ;; Bug
                   ;; https://lists.gnu.org/archive/html/guix-devel/2024-01/msg00042.html
                   ;; Notifying the user is needed for syncthing since no
                   ;; username is otherwise derived
                   (for-home
                    (syncthing-configuration
                     (user "sl"))))
          ;; Pipewire seems needed to share screen
          ;; https://wiki.archlinux.org/title/PipeWire#WebRTC_screen_sharing
          (service home-pipewire-service-type)
          (service home-dbus-service-type)

          ;; XXX: gnome-keyring is out of ssh keys in default build (which seems
          ;; the case in Guix at least now
          ;; orgit-rev:~/Code/System/guix/::9866d32e173050ba99dc520b0a4d5aacb85e3fa0
          ;; https://gitlab.gnome.org/GNOME/gnome-keyring/-/commit/25c5a1982467802fa12c6852b03c57924553ba73
          ;; https://wiki.archlinux.org/title/GNOME/Keyring#Setup_gcr
          ;; TODO: mention on chat, follow updates
          (service home-gpg-agent-service-type
                   (home-gpg-agent-configuration
                    (pinentry-program
                     (file-append pinentry-gnome3 "/bin/pinentry-gnome3"))
                    (ssh-support? #t)))

          ;; TODO: onedrive service
          ))))

;; TODO: check if merged
(define-record-type* <ppd-configuration> ppd-configuration
  make-ppd-configuration ppd-configuration?
  (ppd ppd-configuration-ppd
       (default power-profiles-daemon)))

(define ppd-activation-service
  #~(begin
      (use-modules (guix build utils))
      (mkdir-p "/var/lib/power-profiles-daemon")))

(define (ppd-shepherd-service config)
  (shepherd-service (documentation "Run the power-profiles-daemon.")
                    (provision '(ppd))
                    (requirement '(dbus-system))
                    (start #~(make-forkexec-constructor (list (string-append #$(ppd-configuration-ppd config)
                                                                             "/libexec/power-profiles-daemon"))))
                    (stop #~(make-kill-destructor))))

(define ppd-service-type
  (service-type (name 'ppd)
                (default-value (ppd-configuration))
                (extensions (list (service-extension activation-service-type
                                                     (const
                                                      ppd-activation-service))
                                  (service-extension
                                   shepherd-root-service-type
                                   (compose list ppd-shepherd-service))
                                  (service-extension dbus-root-service-type
                                                     (compose list ppd-configuration-ppd))
                                  (service-extension polkit-service-type
                                                     (compose list ppd-configuration-ppd))
                                  (service-extension profile-service-type
                                                     (compose list ppd-configuration-ppd))))
                (description
                 "Run @command{power-profiles-daemon}.")))

;; udev for betrusted.io precursor
;; https://github.com/betrusted-io/betrusted-wiki/wiki/Updating-Your-Device#linux
(define %precursor-udev-rule
  (udev-rule
   "99-precursor-usb.rules"
   (string-append "SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"1209\", "
                  "ATTRS{idProduct}==\"5bf0\", GROUP=\"plugdev\", "
                  "TAG+=\"uaccess\""
                  "\n"
                  "SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"1209\", "
                  "ATTRS{idProduct}==\"3613\", GROUP=\"plugdev\", "
                  "TAG+=\"uaccess\"")))

(define other-channels
  (append
   (list (channel
          (name 'nonguix)
          (url "https://gitlab.com/nonguix/nonguix")
          (introduction
           (make-channel-introduction
            "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
            (openpgp-fingerprint
             "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
         (channel
          (name 'guix-science)
          (url "https://codeberg.org/guix-science/guix-science.git")
          (introduction
           (make-channel-introduction
            "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
            (openpgp-fingerprint
             "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446")))))
   %default-channels))

(operating-system
 (locale "en_IE.utf8")
 (timezone "Europe/Paris")
 (host-name "beoga")

 (kernel linux)
 (initrd microcode-initrd)
 (firmware (list linux-firmware))

 (keyboard-layout
  ;; TODO: use #:options
  (keyboard-layout "us" "dvorak-alt-intl"))

 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets (list "/boot/efi"))
   (keyboard-layout keyboard-layout)))

 ;; The list of file systems that get "mounted".  The unique
 ;; file system identifiers there ("UUIDs") can be obtained
 ;; by running 'blkid' in a terminal.
 ;; TODO: convert to btrfs
 ;; https://askubuntu.com/questions/198000/how-can-i-convert-an-ext4-partition-to-btrfs-or-other-file-systems-without-los
 (file-systems
  (cons*
   (file-system
    (mount-point "/")
    (device (uuid "045cae44-fd4f-44b2-a83a-0144fa833fa6" 'ext4))
    (type "ext4"))
   (file-system
    (mount-point "/boot/efi")
    (device (uuid "7DBD-DA94" 'fat32))
    (type "vfat"))
   %base-file-systems))

 (swap-devices
  (list
   (swap-space
    (target "/swapfile")
    (dependencies (filter (file-system-mount-point-predicate "/")
                          file-systems)))))

 (kernel-arguments
   (cons* "resume=/dev/nvme0n1p3"    ; root partition which holds /swapfile
          "resume_offset=304244736"  ; offset of /swapfile on the root partition
          ;; Remove screen flickering white
          ;; https://wiki.archlinux.org/title/AMDGPU#Screen_flickering_white_when_using_KDE
          ;; https://www.phoronix.com/news/AMD-Scatter-Gather-Re-Enabled
          "amdgpu.sg_display=0"
          %default-kernel-arguments))

 ;; The list of user accounts ('root' is implicit).
 (users
  (cons*
   (user-account
    (name "sl")
    (comment "Sébastien Lerique")
    (group "users")
    (home-directory "/home/sl")
    (supplementary-groups
     '("wheel" "netdev" "audio" "video"  ; base groups from docs
       "lp"        ; for printing
       "dialout"   ; access to serial devices
       "libvirt"   ; administer VMs
       "kvm"       ; access hardware virtualization features
       "docker"    ; run containers without being root
       "plugdev"   ; udevs plugged in
       "adbusers"  ; interact with android devices
       ))
    (shell #~(string-append #$fish "/bin/fish")))
   %base-user-accounts))

 ;; udevs plugged in
 (groups (cons* (user-group
                 (name "plugdev"))
                %base-groups))

 ;; Below is the list of system services.  To search for available
 ;; services, run 'guix system search KEYWORD' in a terminal.
 (services
  (append (list
           ;; Home
           (service guix-home-service-type
                    `(("sl" ,sl-home-environment)))

           ;; Gnome
           (service gnome-desktop-service-type)
           ;(service gnome-keyring-service-type)
           ;; udev light
           (udev-rules-service 'brillo brillo)
           ;; udev for betrusted.io precursor
           (udev-rules-service 'precursor %precursor-udev-rule)
           ;; ADB
           (udev-rules-service 'android android-udev-rules
                               #:groups '("adbusers"))
           ;; OpenSSH
           (service openssh-service-type)
           ;; Bluetooth
           (service bluetooth-service-type
                    (bluetooth-configuration
                     (name host-name)
                     (auto-enable? #t)))
           ;; Printing
           (service cups-service-type
                    (cups-configuration
                     (web-interface? #t)
                     (extensions
                      (list cups-filters foo2zjs))))
           ;; Kill memory hogs early, before the system starts looking
           ;; for swap
           (service earlyoom-service-type)
           ;; libvirt for VMs
           (service libvirt-service-type
                    (libvirt-configuration
                     (unix-sock-group "libvirt")
                     (listen-tls? #f)
                     (listen-tcp? #f)
                     (min-workers 1)))
           (service virtlog-service-type
                    (virtlog-configuration))
           ;; Docker
           (service docker-service-type)
           (service containerd-service-type)
           ;; Fingerprint
           ;(service fprintd-service-type)
           ;; Power Profile Daemon
           (service ppd-service-type)
           ;; Make scripts with /usr/bin/env work
           (extra-special-file "/usr/bin/env"
                               (file-append coreutils "/bin/env"))
           ;; Keyboard
           (set-xorg-configuration
            (xorg-configuration (keyboard-layout keyboard-layout))))

          (modify-services
           %desktop-services
           (gdm-service-type
            config =>
            (gdm-configuration
             (inherit config)
             (wayland? #t)))
           ;; Use substitutes:
           ;; - nonguix provided by https://substitutes.nonguix.org/
           ;; - guix-science provided by https://guix.bordeaux.inria.fr/
           (guix-service-type
            config =>
            (guix-configuration
             (inherit config)
             (channels other-channels)
             (guix (guix-for-channels other-channels))
             (substitute-urls
              (append
               %default-substitute-urls
               (list "https://substitutes.nonguix.org"
                     "https://guix.bordeaux.inria.fr"
                     )))
             (authorized-keys
              (append
               %default-authorized-guix-keys
               (list (local-file "substitutes.nonguix.org.pub")
                     (local-file "guix.bordeaux.inria.fr.pub")))))))))

 ;; Allow resolution of '.local' host names with mDNS.
 (name-service-switch %mdns-host-lookup-nss))
