(use-modules
 (gnu)
 (gnu packages)
 (gnu system setuid)
 (srfi srfi-1)
 (nongnu packages linux)
 (nongnu system linux-initrd))
(use-package-modules
 android
 certs
 cups
 display-managers
 gnome
 linux
 package-management
 shells
 wm)
(use-service-modules
 cups
 desktop
 docker
 linux
 networking
 nix
 pm
 sddm
 virtualization
 xorg)

(operating-system
 (host-name "giona")
 (timezone "Europe/Paris")
 (locale "en_IE.utf8")

 (kernel linux)
 (initrd microcode-initrd)
 (firmware (cons* iwlwifi-firmware
                 %base-firmware))

 (keyboard-layout
  (keyboard-layout "us" "dvorak-alt-intl"))

 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets '("/boot/efi"))
   (keyboard-layout keyboard-layout)))

 (mapped-devices
  (list (mapped-device
         (source (uuid "7552fdd7-d0c2-4db2-a3d8-fac38a318ad0"))
         (target "cryptroot")
         (type luks-device-mapping))))

 (file-systems
  (cons* (file-system
          (mount-point "/")
          (device "/dev/mapper/cryptroot")
          (type "btrfs")
          (dependencies mapped-devices))
         (file-system
          (mount-point "/boot/efi")
          (device (uuid "7EF1-3CF0" 'fat32))
          (type "vfat"))
         %base-file-systems))

 (users (cons* (user-account
                (name "sl")
                (comment "Sébastien Lerique")
                (group "users")
                (home-directory "/home/sl")
                (supplementary-groups
                 '("wheel" "netdev" "audio" "video" ; base groups from docs
                   "lp"        ; for printing
                   "dialout"   ; access to serial devices
                   "libvirt"   ; administer VMs
                   "kvm"       ; access hardware virtualization features
                   "docker"    ; run containers without being root
                   "adbusers"  ; interact with android devices
                   ))
                (shell #~(string-append #$fish "/bin/fish")))
               (user-account
                (name "sltest")
                (comment "Test Account")
                (group "users")
                (home-directory "/home/sltest")
                (supplementary-groups
                 '("wheel" "netdev" "audio" "video" ; base groups from docs
                   ))
                )
               %base-user-accounts))

 (packages (append (list
                    ;; Desktop environment
                    sway light guix-simplyblack-sddm-theme
                    ;; HTTPS access
                    nss-certs
                    ;; for user mounts
                    gvfs
                    ;; nix offers some missing packages in guix
                    nix)
                   %base-packages))

 (setuid-programs (append (list
                           (setuid-program (program (file-append light "/bin/light")))
                           (setuid-program (program (file-append swaylock "/bin/swaylock"))))
                          %setuid-programs))

 ;; SDDM, Gnome, Bluetooth, Printing
 (services (append (list
                    ;; SDDM
                    (service sddm-service-type
                             (sddm-configuration
                              (display-server "wayland")
                              (session-command (local-file
                                                "./wayland-session"
                                                #:recursive? #t))
                              (theme "guix-simplyblack-sddm")))
                    ;; Gnome and its keyring
                    (service gnome-desktop-service-type)
                    (service gnome-keyring-service-type)
                    ;; Bluetooth
                    (service bluetooth-service-type)
                    ;; Printing
                    (service cups-service-type
                            (cups-configuration
                             (web-interface? #t)
                             (extensions
                              (list cups-filters foo2zjs))))
                    ;; Power management
                    (service tlp-service-type)
                    ;; Make scripts with /usr/bin/env work
                    (extra-special-file "/usr/bin/env"
                                        (file-append coreutils "/bin/env"))
                    ;; Have qemu around to build packages for other
                    ;; architectures
                    (service qemu-binfmt-service-type
                             (qemu-binfmt-configuration
                              (platforms (lookup-qemu-platforms "arm" "aarch64" "mips64el"))))
                    ;; Docker
                    (service docker-service-type)
                    ;; Kill memory hogs early, before the system starts looking
                    ;; for swap
                    (service earlyoom-service-type)
                    ;; nix build daemon
                    (service nix-service-type)
                    ;; libvirt for VMs
                    (service libvirt-service-type
                             (libvirt-configuration
                              (unix-sock-group "libvirt")
                              (listen-tls? #f)
                              (listen-tcp? #f)
                              (min-workers 1)))
                    (service virtlog-service-type
                             (virtlog-configuration))
                    ;; acpi_call kernel module for battery control
                    (service kernel-module-loader-service-type
                             '("acpi_call"))
                    ;; allow members of adbusers to interact with android
                    ;; devices without root permissions
                    (udev-rules-service 'android android-udev-rules
                                        #:groups '("adbusers")))
                   (modify-services
                    ;; No need for GDM
                    (remove (lambda (service)
                              (member (service-kind service)
                                      (list gdm-service-type modem-manager-service-type)))
                            %desktop-services)
                    ;; Use substitutes:
                    ;; - nonguix provided by https://substitutes.nonguix.org/
                    ;; - guix-science provided by https://substitutes.guix.psychnotebook.org/
                    (guix-service-type
                     config =>
                     (guix-configuration
                      (inherit config)
                      (substitute-urls
                       (append
                        %default-substitute-urls
                        (list "https://substitutes.nonguix.org"
                              "https://guix.bordeaux.inria.fr"
                              )))
                      (authorized-keys
                       (append
                        %default-authorized-guix-keys
                        (list (local-file "substitutes.nonguix.org.pub")
                              (local-file "guix.bordeaux.inria.fr.pub")
                              ))))))))

 ;; Be able to load the acpi_call kernel module
 (kernel-loadable-modules (list acpi-call-linux-module))

 ;; Allow resolution of '.local' host names with mDNS.
 (name-service-switch %mdns-host-lookup-nss))
