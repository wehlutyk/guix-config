(use-modules (gnu packages gcc))

(concatenate-manifests
 (list
  (specifications->manifest
   '("avr-toolchain"
     "dfu-programmer"
     "gcc-toolchain"
     "glibc"
     "icedtea"
     "libx11"
     "libxrandr"
     "libxtst"
     "python"
     "python-pyserial"
     "man-db"))
  (packages->manifest
   `((,gcc "lib")))))
